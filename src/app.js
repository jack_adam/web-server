const path = require("path");
const express = require("express");
const hbs = require("hbs");
const forecast = require("./utils/forecast");
const geocode = require("./utils/geocode");

const port = process.env.PORT || 3000;
const app = express();
const partials_Path = path.join(__dirname, "../views/partials");

app.use(express.static("public"));
app.set("view engine", "hbs");
hbs.registerPartials(partials_Path);

app.get("", (req, res) => {
  res.render("index", {
    title: "Home",
  });
});

app.get("/about", (req, res) => {
  res.render("about", {
    title: "About",
    author: "Jack Adam",
  });
});

app.get("/weather", (req, res) => {
  if (!req.query.address)
    return res.send({
      error: " You Must Provide An address ",
      address: req.query.address,
    });

  geocode(
    req.query.address,
    (error, { longitude, latitude, location } = {}) => {
      if (error) {
        return res.send({
          error,
          address: `[ ${req.query.address} ] Is Not A Valid Location `,
        });
      }

      forecast(longitude, latitude, (error, forecastData) => {
        if (error) return res.send({ error });
        res.send({ forecast: forecastData, address: location });
      });
    },
  );
});

app.get("/*", (req, res) => {
  res.render("404", {
    title: "Error 404",
    errorMessage: "Page Not Found",
  });
});

app.listen(port, () => {
  console.log(`Server Is Up At Port ${port}`);
});
